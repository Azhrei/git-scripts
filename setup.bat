@ECHO OFF


set acp=git config --global alias.acp "!%CD%\windows\acp.bat"
set "acp=%acp:\=/%"
%acp%

set fixup=git config --global alias.fixup "!%CD%\windows\fixup.bat"
set "fixup=%fixup:\=/%"
%fixup%

set pr=git config --global alias.pr "!%CD%\windows\pr.bat"
set "pr=%pr:\=/%"
%pr%