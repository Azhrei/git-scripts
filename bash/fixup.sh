#!/usr/bin/env bash

git add .
git commit -m "rm"
git rebase -i HEAD~2

$branch=`git rev-parse --abbrev-ref HEAD`

while true; do
    read -p "Are you sure you want to force push $branch?" yn
    case $yn in
        [Yy]* ) git push --force origin $branch; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
