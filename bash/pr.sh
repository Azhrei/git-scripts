#!/usr/bin/env bash

git checkout -b $1
git add .
git commit -m $2
git push -u origin $1