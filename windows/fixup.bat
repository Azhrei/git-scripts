@ECHO OFF

git add .
git commit -m "rm"
git rebase -i HEAD~2

for /f %%i in ('git rev-parse --abbrev-ref HEAD') do set current-branch=%%i 

:choice
set /P c=Are you sure you want to force push %current-branch%[Y/N]?
if /I "%c%" EQU "Y" goto :push
if /I "%c%" EQU "N" goto :exit
goto :choice

:push
git push --force origin %current-branch%

:exit
