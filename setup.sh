#!/usr/bin/env bash
echo "This is as yet untested, and likely needs to have its line endings fixed"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

git config --global alias.acp "!sh ${DIR}/bash/acp.bat"
git config --global alias.fixup "!sh ${DIR}/bash/fixup.bat"
git config --global alias.pr "!sh ${DIR}/bash/pr.bat"
